﻿using Data.Repositories;
using Domain.Entities;
using Site.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site.Helpers
{
    public class CompetitionHelper
    {
        protected readonly MatchRepository _matchRepository = new MatchRepository();
        protected readonly MatchHelper _matchHelper = new MatchHelper();
        protected readonly TeamRepository _teamRepository = new TeamRepository();
        public IEnumerable<CompetitionByTeamViewModel> GetCompetitionsDetails(int teamId)
        {
            //var competitions = originalMatches.Select(x => new Competition { CompetitionId = x.CompetitionId, Name = x.Competition.Name }).Distinct();
            var resultList = new List<CompetitionByTeamViewModel>();
            //foreach (var competition in competitions)
            //{
            //    var result = new CompetitionByTeamViewModel();
            //    var matches = _matchRepository.GetMatchesByTeamByComp(teamId, competition.CompetitionId);
            //    var teamdata = _matchHelper.TeamData(matches, _teamRepository.GetById(teamId));
            //    result.Competition = competition;
            //    result.Played = matches.Where(x => x.Status == MatchStatus.Finished).Count();
            //    result.Scheduled = matches.Where(x => x.Status == MatchStatus.Scheduled).Count();
            //    result.Victories = _matchHelper.TeamWins(matches, teamId);
            //    result.Draws = _matchHelper.Draws(matches);
            //    result.Losses = result.Played - result.Victories - result.Draws;
            //    result.GoalsScored = teamdata.Goals;
            //    result.GoalsAgainst = teamdata.GoalsAgainst;
            //    resultList.Add(result);
            //}

            return resultList;
        }
    }
}
