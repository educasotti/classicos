﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Site.Helpers
{
    public class FileUploadHelper
    {
        public async Task<string> UploadFile(string caminho, IFormFile file, string name)
        {
            var nomeArquivo = "";

            if (file == null || file.Length == 0)
                return null;

            var path = Path.Combine(
                        Directory.GetCurrentDirectory(), caminho,
                        name);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            return nomeArquivo;
        }


    }
}
