﻿using Data.Repositories;
using Domain.Entities;
using Site.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site.Helpers
{
    public class MatchHelper
    {

        protected readonly MatchRepository _matchRepository = new MatchRepository();
        protected readonly CompetitionRepository _compRepository = new CompetitionRepository();
        protected readonly TeamRepository _teamRepository = new TeamRepository();
        public int TeamWins(IEnumerable<Match> matches, int teamId)
        {
            var count = 0;
            foreach (var match in matches)
            {
                if ((match.HomeTeamId == teamId && match.ScoreHome > match.ScoreAway) || (match.AwayTeamId == teamId && match.ScoreAway > match.ScoreHome))
                    count++;
            }
            return count;
        }

        public int TeamGoals(IEnumerable<Match> matches, int teamId)
        {
            var count = 0;
            foreach (var match in matches)
            {
                if (match.HomeTeamId == teamId)
                    count += match.ScoreHome;
                if (match.AwayTeamId == teamId)
                    count += match.ScoreAway;
            }
            return count;
        }

        public TeamCompareViewModel TeamData(IEnumerable<Match> matches, Team team)
        {
            var countWins = 0;
            var countGoals = 0;
            var countGoalsAgainst = 0;
            foreach (var match in matches)
            {
                if ((match.HomeTeamId == team.TeamId && match.ScoreHome > match.ScoreAway) || (match.AwayTeamId == team.TeamId && match.ScoreAway > match.ScoreHome))
                    countWins++;

                if (match.HomeTeamId == team.TeamId)
                {
                    countGoals += match.ScoreHome;
                    countGoalsAgainst += match.ScoreAway;
                }

                if (match.AwayTeamId == team.TeamId)
                {
                    countGoals += match.ScoreAway;
                    countGoalsAgainst += match.ScoreHome;
                }
            }
            return new TeamCompareViewModel
            {
                Team = team,
                Wins = countWins,
                Goals = countGoals,
                GoalsAgainst = countGoalsAgainst
            };
        }

        public int Draws(IEnumerable<Match> matches)
        {
            var count = 0;
            foreach (var match in matches)
            {
                if (match.ScoreAway == match.ScoreHome && match.Status == MatchStatus.Finished)
                    count++;
            }
            return count;
        }

        public int Played(IEnumerable<Match> matches)
        {
            return matches.Where(x => x.Status == MatchStatus.Finished).Count();
        }

        public int Scheduled(IEnumerable<Match> matches)
        {
            return matches.Where(x => x.Status == MatchStatus.Scheduled).Count();
        }

        public TeamDetailViewModel GetDetails(Team t, bool isDetail)
        {            
            var model = new TeamDetailViewModel
            {
                TeamId = t.TeamId,
                ConfederationId = t.ConfederationId,
                Confederation = t.Confederation,
                FullName = t.FullName,
                NickName = t.NickName,
                TeamCode = t.TeamCode,
                HomeAt = t.HomeAt,
                EstablishedAt = t.EstablishedAt,
                Name = t.Name,
                Crest = t.Crest,
                Matches = _matchRepository.GetFiltered(t.TeamId, 0, "")
            };
            var competitions = model.Matches.Select(x => x.CompetitionId).Distinct();            
            model.TotalPlayed = model.Matches.Where(x => x.Status == MatchStatus.Finished).Count();
            model.TotalScheduled = model.Matches.Where(x => x.Status == MatchStatus.Scheduled).Count();
            model.Competitions = GetCompetitionsDetails(t.TeamId, competitions);

            var teamStats = TeamData(model.Matches, t);
            model.Wins = teamStats.Wins;
            if (model.TotalPlayed == 0 || model.Wins == 0)
                model.PerformanceRecord = 0.00;
            else
                model.PerformanceRecord = ((double)model.Wins * 100) / (double)model.TotalPlayed;

            model.Draws = Draws(model.Matches);
            model.Losses = model.TotalPlayed - model.Wins - model.Draws;
            model.GoalsScored = teamStats.Goals;
            model.GoalsAgainst = teamStats.GoalsAgainst;
            model.GoalDifference = model.GoalsScored - model.GoalsAgainst;
            if (model.GoalsScored == 0)
                model.GoalsScoredAverage = 0.00;
            else
                model.GoalsScoredAverage = ((double)model.GoalsScored / (double)model.TotalPlayed);

            if (model.GoalsAgainst == 0)
                model.GoalsAgainstAverage = 0.00;
            else
                model.GoalsAgainstAverage = ((double)model.GoalsAgainst / (double)model.TotalPlayed);

            if (isDetail)
            {
                model.FirstMatch = _matchRepository.TeamFirstMatch(t.TeamId);
                model.LastMatch = _matchRepository.TeamLastMatch(t.TeamId);
                model.NextMatch = _matchRepository.TeamNextMatch(t.TeamId);
                model.BiggestWin = _matchRepository.BiggestWin(t.TeamId);
                model.BiggestDefeat = _matchRepository.BiggestDefeat(t.TeamId);
            }
            return model;
        }
        public IEnumerable<CompetitionByTeamViewModel> GetCompetitionsDetails(int teamId, IEnumerable<int> competitions)
        {            
            var resultList = new List<CompetitionByTeamViewModel>();
            foreach (var competition in competitions)
            {
                var result = new CompetitionByTeamViewModel();
                var matches = _matchRepository.GetMatchesByTeamByComp(teamId, competition);
                var teamdata = TeamData(matches, _teamRepository.GetById(teamId));
                result.Competition = _compRepository.GetById(competition);
                result.Played = matches.Where(x => x.Status == MatchStatus.Finished).Count();
                result.Scheduled = matches.Where(x => x.Status == MatchStatus.Scheduled).Count();
                result.Victories = TeamWins(matches, teamId);
                result.Draws = Draws(matches);
                result.Losses = result.Played - result.Victories - result.Draws;
                result.GoalsScored = teamdata.Goals;
                result.GoalsAgainst = teamdata.GoalsAgainst;
                resultList.Add(result);
            }

            return resultList;
        }
    }
}
