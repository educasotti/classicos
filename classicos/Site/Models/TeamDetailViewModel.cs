﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site.Models
{
    public class TeamDetailViewModel
    {
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string TeamCode { get; set; }
        public string Crest { get; set; }
        public IEnumerable<Match> Matches { get; set; }
        public int TotalPlayed { get; set; }
        public int TotalScheduled { get; set; }
        public int ConfederationId { get; set; }
        public string FullName { get; set; }
        public string NickName { get; set; }
        public virtual Confederation Confederation { get; set; }
        public DateTime EstablishedAt { get; set; }
        public double PerformanceRecord { get; set; }
        public string HomeAt { get; set; }
        public int Wins { get; set; }
        public int Draws { get; set; }
        public int Losses { get; set; }
        public int GoalsScored { get; set; }
        public int GoalsAgainst { get; set; }
        public int GoalDifference { get; set; }
        public double GoalsScoredAverage { get; set; }
        public double GoalsAgainstAverage { get; set; }
        public Match FirstMatch { get; set; }
        public Match LastMatch { get; set; }
        public Match NextMatch { get; set; }
        public Match BiggestWin { get; set; }
        public Match BiggestDefeat { get; set; }

        public IEnumerable<CompetitionByTeamViewModel> Competitions { get; set; }
    }
}
