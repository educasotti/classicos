﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site.Models
{
    public class CompetitionByTeamViewModel
    {
        public int TeamId { get; set; }
        public Competition Competition { get; set; }
        public int Played { get; set; }
        public int Scheduled { get; set; }
        public int Victories { get; set; }
        public int Draws { get; set; }
        public int Losses { get; set; }
        public int GoalsScored { get; set; }
        public int GoalsAgainst { get; set; }
    }
}
