﻿using System;
using Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Site.Models
{
    public class MatchIndexViewModel
    {
        public IEnumerable<Match> Matches { get; set; }
        public SelectList Teams { get; set; }
        public SelectList Competitions { get; set; }
        public IEnumerable<MatchStatus> MatchStatuses { get; set; }

        public MatchIndexViewModel(IEnumerable<Match> matches)
        {
            Matches = matches;
        }
    }

    public class MatchTeamsViewModel
    {
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
