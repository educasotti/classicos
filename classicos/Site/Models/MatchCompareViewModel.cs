﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Site.Models
{
    public class MatchCompareViewModel
    {
        public TeamCompareViewModel Team1 { get; set; }
        public TeamCompareViewModel Team2 { get; set; }
        public IEnumerable<Match> Matches { get; set; }        
        public int Draws { get; set; }
        public int MatchesPlayed { get; set; }
        public int MatchesScheduled { get; set; }

        public MatchCompareViewModel()
        {
            Matches = new List<Match>();
            Team1 = null;
            Team2 = null;
        }
    }

    public class TeamCompareViewModel
    {
        public Team Team { get; set; }
        public int Wins { get; set; }
        public int Goals { get; set; }
        public int GoalsAgainst { get; set; }

        public TeamCompareViewModel()
        {
            
        }
        public TeamCompareViewModel(Team team)
        {
            Team = team;
        }
    }
}
