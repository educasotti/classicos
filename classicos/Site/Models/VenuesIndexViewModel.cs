﻿using System;
using Domain.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Site.Models
{
    public class VenuesIndexViewModel
    {
        public IEnumerable<Domain.Entities.Venue> Venues { get; set; }
        public SelectList Teams { get; set; }
        public SelectList Confederations { get; set; }
        public SelectList Localities { get; set; }

        public VenuesIndexViewModel(IEnumerable<Venue> venues)
        {
            Venues = venues;
        }
    }
}
