﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Data.Context;
using Domain.Entities;
using Data.Repositories;
using Site.Models;
using Site.Helpers;

namespace Site.Controllers
{
    public class MatchesController : Controller
    {
        private readonly ClassicosContext _context;
        protected readonly TeamRepository _teamRepository = new TeamRepository();
        protected readonly CompetitionRepository _competitionRepository = new CompetitionRepository();
        protected readonly MatchRepository _matchRepository = new MatchRepository();
        protected readonly MatchHelper _matchHelper = new MatchHelper();

        public MatchesController(ClassicosContext context)
        {
            _context = context;
        }

        public JsonResult GetAutoCompleteTeams()
        {
            var teams = new List<MatchTeamsViewModel>();
            foreach(var team in _teamRepository.GetMany(x => x.IsPublished))
            {
                teams.Add(new MatchTeamsViewModel
                {
                    TeamId = team.TeamId,
                    Code = team.TeamCode,
                    Name = team.Name
                });
            }
            return Json(teams);
        }

        // GET: Matches
        public IActionResult Index(int teamId = 0, int competitionId = 0, string scStatus = "", int page=1)
        {
            var skip = (page - 1) * 15;
            var take = 15;
            var count = _matchRepository.GetFiltered(teamId, competitionId, scStatus).Count();
            ViewBag.TotalPages = (int)Math.Ceiling(Decimal.Divide(count, take));
            ViewBag.Count = count;
            var model = new MatchIndexViewModel(_matchRepository.GetFiltered(teamId, competitionId, scStatus.ToLower()).OrderBy(x => x.Date).Skip(skip).Take(take));
            model.Teams = new SelectList(_teamRepository.GetMany(x => x.IsPublished), "TeamId", "Name", teamId);
            model.Competitions = new SelectList(_competitionRepository.GetMany(x => x.IsPublished), "CompetitionId", "Name", competitionId);            
            return View(model);
        }

        public IActionResult Compare(int team1 = 0, int team2 = 0)
        {
            ViewData["Team1"] = new SelectList(_teamRepository.GetMany(x => x.IsPublished), "TeamId", "Name", team1);
            ViewData["Team2"] = new SelectList(_teamRepository.GetMany(x => x.IsPublished), "TeamId", "Name", team2);

            if (team1 == 0 || team2 == 0)            
                return View(new MatchCompareViewModel());

            var matches = _matchRepository.Compare(team1, team2);
            
            
            return View(new MatchCompareViewModel
                            {
                                Matches = matches,
                                Team1 = _matchHelper.TeamData(matches, _teamRepository.GetById(team1)),
                                Team2 = _matchHelper.TeamData(matches, _teamRepository.GetById(team2)),
                                Draws = _matchHelper.Draws(matches),
                                MatchesPlayed = _matchHelper.Played(matches),
                                MatchesScheduled = _matchHelper.Scheduled(matches)
                            }
            );
        }

        // GET: Matches/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var match = await _context.Matches
                .Include(m => m.AwayTeam)
                .Include(m => m.Competition)
                .Include(m => m.HomeTeam)
                .FirstOrDefaultAsync(m => m.MatchId == id);
            if (match == null)
            {
                return NotFound();
            }

            return View(match);
        }

        // GET: Matches/Create
        public IActionResult Create()
        {
            ViewData["AwayTeamId"] = new SelectList(_context.Teams, "TeamId", "Name");
            ViewData["CompetitionId"] = new SelectList(_context.Competitions, "CompetitionId", "Name");
            ViewData["HomeTeamId"] = new SelectList(_context.Teams, "TeamId", "Name");
            var status = new List<MatchStatus>();
            status.Add(MatchStatus.Scheduled);
            status.Add(MatchStatus.Finished);
            ViewData["Status"] = new SelectList(status, "Status", "Status");
            return View();
        }

        // POST: Matches/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MatchId,HomeTeamId,AwayTeamId,ScoreHome,ScoreAway,AET,PK,PKScore,Date,CompetitionId,Stage,Venue,Locality,Status,Created,Updated,IsPublished,IsDeleted")] Match match)
        {
            if (ModelState.IsValid)
            {
                if(match.HomeTeamId != match.AwayTeamId)
                {
                    match.Created = match.Updated = DateTime.Now;
                    _context.Add(match);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Create));
                }
                else
                {
                    ModelState.AddModelError("sameteams", "Same teams");
                }
                
            }
            ViewData["AwayTeamId"] = new SelectList(_context.Teams, "TeamId", "Name", match.AwayTeamId);
            ViewData["CompetitionId"] = new SelectList(_context.Competitions, "CompetitionId", "Name", match.CompetitionId);
            ViewData["HomeTeamId"] = new SelectList(_context.Teams, "TeamId", "Name", match.HomeTeamId);
            var status = new List<MatchStatus>();
            status.Add(MatchStatus.Scheduled);
            status.Add(MatchStatus.Finished);
            ViewData["Status"] = new SelectList(status, "Status", "Status");
            return View(match);
        }

        // GET: Matches/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var match = await _context.Matches.FindAsync(id);
            if (match == null)
            {
                return NotFound();
            }
            ViewData["AwayTeamId"] = new SelectList(_context.Teams, "TeamId", "Name", match.AwayTeamId);
            ViewData["CompetitionId"] = new SelectList(_context.Competitions, "CompetitionId", "Name", match.CompetitionId);
            ViewData["HomeTeamId"] = new SelectList(_context.Teams, "TeamId", "Name", match.HomeTeamId);
            var status = new List<MatchStatus>();
            status.Add(MatchStatus.Scheduled);
            status.Add(MatchStatus.Finished);
            ViewData["Status"] = new SelectList(status, "Status", "Status");
            match.AwayTeam = _context.Teams.SingleOrDefault(x => x.TeamId == match.AwayTeamId);
            match.HomeTeam = _context.Teams.SingleOrDefault(x => x.TeamId == match.HomeTeamId);
            return View(match);
        }

        // POST: Matches/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MatchId,HomeTeamId,AwayTeamId,ScoreHome,ScoreAway,Date,CompetitionId,Stage,Venue,Locality,Status,Created,Updated,AET, PK, PKScore, IsPublished,IsDeleted")] Match match)
        {
            if (id != match.MatchId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    match.Updated = DateTime.Now;
                    _context.Update(match);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MatchExists(match.MatchId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Compare", new { team1 = match.HomeTeamId, team2 = match.AwayTeamId});
            }
            ViewData["AwayTeamId"] = new SelectList(_context.Teams, "TeamId", "Crest", match.AwayTeamId);
            ViewData["CompetitionId"] = new SelectList(_context.Competitions, "CompetitionId", "Name", match.CompetitionId);
            ViewData["HomeTeamId"] = new SelectList(_context.Teams, "TeamId", "Crest", match.HomeTeamId);
            var status = new List<MatchStatus>();
            status.Add(MatchStatus.Scheduled);
            status.Add(MatchStatus.Finished);
            ViewData["Status"] = new SelectList(status, "Status", "Status");
            return View(match);
        }

        // GET: Matches/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var match = await _context.Matches
                .Include(m => m.AwayTeam)
                .Include(m => m.Competition)
                .Include(m => m.HomeTeam)
                .FirstOrDefaultAsync(m => m.MatchId == id);
            if (match == null)
            {
                return NotFound();
            }

            return View(match);
        }

        // POST: Matches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var match = await _context.Matches.FindAsync(id);
            _context.Matches.Remove(match);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MatchExists(int id)
        {
            return _context.Matches.Any(e => e.MatchId == id);
        }
    }
}
