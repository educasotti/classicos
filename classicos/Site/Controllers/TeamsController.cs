﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Data.Context;
using Domain.Entities;
using Data.Repositories;
using Site.Helpers;
using Microsoft.AspNetCore.Http;
using Site.Models;

namespace Site.Controllers
{
    public class TeamsController : Controller
    {
        private readonly ClassicosContext _context;
        private readonly TeamRepository _teamRepository = new TeamRepository();
        private readonly ConfederationRepository _confRepository = new ConfederationRepository();
        private readonly FileUploadHelper _fileUploadHelper = new FileUploadHelper();
        protected readonly MatchRepository _matchRepository = new MatchRepository();
        protected readonly MatchHelper _matchHelper = new MatchHelper();

        public TeamsController(ClassicosContext context)
        {
            _context = context;
        }

        // GET: Teams
        [ResponseCache(Duration =1200)]
        public IActionResult Index(string order = "", int confederation = 0)
        {
            
            var teams = _teamRepository.GetTeams(confederation);
            var output = new List<TeamDetailViewModel>();

            foreach(var t in teams)                
                output.Add(_matchHelper.GetDetails(t, false));           

            switch(order)
            {
                case "perf":
                    output = output.OrderByDescending(x => x.PerformanceRecord).ToList();
                    break;
                case "wins":
                    output = output.OrderByDescending(x => x.Wins).ToList();
                    break;
                case "matches":
                    output = output.OrderByDescending(x => x.TotalPlayed).ToList();
                    break;
                case "draws":
                    output = output.OrderByDescending(x => x.Draws).ToList();
                    break;
                case "losses":
                    output = output.OrderByDescending(x => x.Losses).ToList();
                    break;
                case "goalsscored":
                    output = output.OrderByDescending(x => x.GoalsScored).ToList();
                    break;
                case "goalsagainst":
                    output = output.OrderByDescending(x => x.GoalsAgainst).ToList();
                    break;
                case "goaldiff":
                    output = output.OrderByDescending(x => x.GoalDifference).ToList();
                    break;
                case "goalsscoredaverage":
                    output = output.OrderByDescending(x => x.GoalsScoredAverage).ToList();
                    break;
                case "goalsagainstaverage":
                    output = output.OrderByDescending(x => x.GoalsAgainstAverage).ToList();
                    break;
                case "est":
                    output = output.OrderBy(x => x.EstablishedAt).ToList();
                    break;
                default:
                    output = output.OrderBy(x => x.Name).ToList();
                    break;
            }          

            return View(output);
        }

        // GET: Teams/Details/5
        public IActionResult Details(string id)
        {
            int intId = 0;
            Team team;
            int.TryParse(id, out intId);
            if (intId == 0 && id == string.Empty)            
                return NotFound();
            else
            {
                if (intId != 0)
                    team = _teamRepository.GetById(intId);                
                else                
                    team = _teamRepository.GetTeamByCode(id);


                if (team == null)
                    return NotFound();

                team.Confederation = _confRepository.GetById(team.ConfederationId);


            }

            

            return View(_matchHelper.GetDetails(team, true));
        }

        // GET: Teams/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Teams/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TeamId,Name,Crest,EmojiFlag,TeamCode,IsPublished")] Team team, IFormFile file)
        {
            if (ModelState.IsValid)
            {
                team.Crest = team.Name + ".png";
                var ok = await _fileUploadHelper.UploadFile("wwwroot\\assets\\teams", file, team.Crest);                
                team.Created = DateTime.Now;
                team.Updated = DateTime.Now;
                team.TeamCode = team.TeamCode.ToUpper();
                _context.Add(team);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(team);
        }

        // GET: Teams/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var team = await _context.Teams.SingleOrDefaultAsync(m => m.TeamId == id);
            if (team == null)
            {
                return NotFound();
            }
            return View(team);
        }

        // POST: Teams/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("TeamId,ConfederationId,EstablishedAt,HomeAt,NickName,FullName,Name,Crest,EmojiFlag,TeamCode,IsPublished")] Team team)
        {
            if (id != team.TeamId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var ok = "";
                    if (Request.Form.Files.Count() > 0)
                       ok = await _fileUploadHelper.UploadFile("wwwroot\\assets\\teams", Request.Form.Files[0], team.Crest);

                    team.Updated = DateTime.Now;
                    team.TeamCode = team.TeamCode.ToUpper();
                    _context.Update(team);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TeamExists(team.TeamId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", new { id = team.TeamId});
            }
            return View(team);
        }

        // GET: Teams/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var team = await _context.Teams
                .SingleOrDefaultAsync(m => m.TeamId == id);
            if (team == null)
            {
                return NotFound();
            }

            return View(team);
        }

        // POST: Teams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var team = await _context.Teams.SingleOrDefaultAsync(m => m.TeamId == id);
            _context.Teams.Remove(team);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TeamExists(int id)
        {
            return _context.Teams.Any(e => e.TeamId == id);
        }
        
    }
}
