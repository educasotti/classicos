﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Data.Context;
using Domain.Entities;

namespace Site.Controllers
{
    public class ConfederationsController : Controller
    {
        private readonly ClassicosContext _context;

        public ConfederationsController(ClassicosContext context)
        {
            _context = context;
        }

        // GET: Confederations
        public async Task<IActionResult> Index()
        {
            return View(await _context.Confederations.ToListAsync());
        }

        // GET: Confederations/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var confederation = await _context.Confederations
                .FirstOrDefaultAsync(m => m.ConfederationId == id);
            if (confederation == null)
            {
                return NotFound();
            }

            return View(confederation);
        }

        // GET: Confederations/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Confederations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ConfederationId,Name,Created,Updated,IsPublished,IsDeleted")] Confederation confederation)
        {
            if (ModelState.IsValid)
            {
                confederation.Created = DateTime.Now;
                confederation.Updated = DateTime.Now;
                _context.Add(confederation);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(confederation);
        }

        // GET: Confederations/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var confederation = await _context.Confederations.FindAsync(id);
            if (confederation == null)
            {
                return NotFound();
            }
            return View(confederation);
        }

        // POST: Confederations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ConfederationId,Name,Created,Updated,IsPublished,IsDeleted")] Confederation confederation)
        {
            if (id != confederation.ConfederationId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    confederation.Updated = DateTime.Now;
                    _context.Update(confederation);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ConfederationExists(confederation.ConfederationId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(confederation);
        }

        // GET: Confederations/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var confederation = await _context.Confederations
                .FirstOrDefaultAsync(m => m.ConfederationId == id);
            if (confederation == null)
            {
                return NotFound();
            }

            return View(confederation);
        }

        // POST: Confederations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var confederation = await _context.Confederations.FindAsync(id);
            _context.Confederations.Remove(confederation);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ConfederationExists(int id)
        {
            return _context.Confederations.Any(e => e.ConfederationId == id);
        }
    }
}
