﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Data.Context;
using Domain.Entities;
using Data.Repositories;
using Site.Models;

namespace Site.Controllers
{
    public class VenuesController : Controller
    {
        private readonly ClassicosContext _context;
        protected readonly TeamRepository _teamRepository = new TeamRepository();
        protected readonly MatchRepository _matchRepository = new MatchRepository();
        protected readonly ConfederationRepository _confRepository = new ConfederationRepository();
        protected readonly VenueRepository _venueRepository = new VenueRepository();

        public VenuesController(ClassicosContext context)
        {
            _context = context;
        }

        // GET: Venues
        public IActionResult Index(int page = 1, int teamId = 0, int confederationId = 0, string locality = "")
        {
            var skip = (page - 1) * 15;
            var take = 15;            
            
            var model = new VenuesIndexViewModel(_venueRepository.GetFiltered(teamId,confederationId, locality).Skip(skip).Take(take));
            var count = _venueRepository.GetFiltered(teamId, confederationId, locality).Count();
            ViewBag.TotalPages = (int)Math.Ceiling(Decimal.Divide(count, take));
            ViewBag.Count = count;
            model.Teams = new SelectList(_teamRepository.GetMany(x => x.IsPublished), "TeamId", "Name", teamId);
            model.Confederations = new SelectList(_confRepository.GetMany(x => x.IsPublished), "ConfederationId", "Name", confederationId);
            model.Localities = new SelectList(_venueRepository.GetLocalities(), locality);
            return View(model);
        }

        // GET: Venues/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var venue = await _context.Venues
                .Include(v => v.Team)
                .FirstOrDefaultAsync(m => m.VenueId == id);
            if (venue == null)
            {
                return NotFound();
            }

            return View(venue);
        }

        // GET: Venues/Create
        public IActionResult Create()
        {
            ViewData["TeamId"] = new SelectList(_context.Teams, "TeamId", "Name");
            return View();
        }

        // POST: Venues/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("VenueId,Name,Locality,Capacity,TeamId,Created,Updated,IsPublished,IsDeleted")] Venue venue)
        {
            if (ModelState.IsValid)
            {
                _context.Add(venue);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["TeamId"] = new SelectList(_context.Teams, "TeamId", "Crest", venue.TeamId);
            return View(venue);
        }

        // GET: Venues/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var venue = await _context.Venues.FindAsync(id);
            if (venue == null)
            {
                return NotFound();
            }
            ViewData["TeamId"] = new SelectList(_context.Teams, "TeamId", "Crest", venue.TeamId);
            return View(venue);
        }

        // POST: Venues/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("VenueId,Name,Locality,Capacity,TeamId,Created,Updated,IsPublished,IsDeleted")] Venue venue)
        {
            if (id != venue.VenueId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(venue);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VenueExists(venue.VenueId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["TeamId"] = new SelectList(_context.Teams, "TeamId", "Crest", venue.TeamId);
            return View(venue);
        }

        // GET: Venues/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var venue = await _context.Venues
                .Include(v => v.Team)
                .FirstOrDefaultAsync(m => m.VenueId == id);
            if (venue == null)
            {
                return NotFound();
            }

            return View(venue);
        }

        // POST: Venues/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var venue = await _context.Venues.FindAsync(id);
            _context.Venues.Remove(venue);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool VenueExists(int id)
        {
            return _context.Venues.Any(e => e.VenueId == id);
        }

        public JsonResult LoadData(int id)
        {
            try
            {
                var c = _teamRepository.GetById(id);
                var m = _matchRepository.GetMatchesByLoc(c.TeamCode);
                int cont = 0;
                foreach (var item in m)
                {
                    _venueRepository.Add(new Venue
                    {
                        Capacity = 10000,
                        Created = DateTime.Now,
                        Updated = DateTime.Now,
                        IsPublished = true,
                        TeamId = id,
                        Locality = item.Locality,
                        Name = item.Venue,
                    });
                    cont++;
                }
                return Json(new { Country = c.TeamCode, Total = cont, venues = m });
            }
            catch
            {
                return Json(new { Error = "Country not Found" });
            }
            
        }
    }
}
