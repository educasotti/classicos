﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.EntityConfig
{
    public class TeamConfiguration : IEntityTypeConfiguration<Team>
    {
        public void Configure (EntityTypeBuilder<Team> builder)
        {
            builder.HasKey(t => t.TeamId);

            builder.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(t => t.Crest)
                .IsRequired()
                .HasMaxLength(500);

            builder.Property(t => t.EmojiFlag)
                .IsRequired(false)
                .HasMaxLength(10);

            builder.Property(t => t.TeamCode)
                .IsRequired(false)
                .HasMaxLength(3); 

        }
    }
}
