﻿using System;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;
using System.Text;

namespace Data.EntityConfig
{
    public class CompetitionStageConfiguration : IEntityTypeConfiguration<CompetitionStage>
    {
        public void Configure(EntityTypeBuilder<CompetitionStage> builder)
        {
            builder.HasKey(cs => cs.CompetitionStageId);
            builder.Property(cs => cs.Name)
                .IsRequired()
                .HasMaxLength(50);
        }
    }
}
