﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.EntityConfig
{
    public class MatchConfiguration : IEntityTypeConfiguration<Match>
    {
        public void Configure(EntityTypeBuilder<Match> builder)
        {
            builder.HasKey(m => m.MatchId);           
            
            builder.Property(m => m.HomeTeamId)
                .IsRequired();
            builder.Property(m => m.ScoreHome)
                .IsRequired();
            builder.Property(m => m.ScoreAway)
                .IsRequired();
            builder.Property(m => m.AwayTeamId)                
                .IsRequired();
            builder.Property(m => m.CompetitionId)
                .IsRequired();
            builder.Property(m => m.Date)
                .IsRequired();
            builder.Property(m => m.Venue)
                .IsRequired()
                .HasMaxLength(255);
            builder.Property(m => m.Stage)
                .IsRequired()
                .HasMaxLength(50);
            builder.Property(m => m.Locality)
                .IsRequired()
                .HasMaxLength(255);
            builder.Property(m => m.AET)
                .IsRequired();
            builder.Property(m => m.PK)
                .IsRequired();            
        }
    }
}
