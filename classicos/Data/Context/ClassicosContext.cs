﻿using Data.EntityConfig;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;
using System.Collections.Generic;

namespace Data.Context
{
    public class ClassicosContext : DbContext
    {
        public ClassicosContext(DbContextOptions<ClassicosContext> options)
        : base(options)
        { 
        }

        public DbSet<Domain.Entities.Team> Teams { get; set; }
        public DbSet<Domain.Entities.Competition> Competitions { get; set; }
        public DbSet<Domain.Entities.Match> Matches { get; set; }
        public DbSet<Domain.Entities.Venue> Venues { get; set; }
        public DbSet<Domain.Entities.Confederation> Confederations { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=.\\SqlExpress;Database=ClassicosDB;User Id=sa;Password=Eduardo@2412;MultipleActiveResultSets=True");

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Domain.Entities.Team>(new TeamConfiguration().Configure);
            modelBuilder.Entity<Domain.Entities.Competition>(new CompetitionConfiguration().Configure);
            modelBuilder.Entity<Domain.Entities.Match>(new MatchConfiguration().Configure);
            modelBuilder.Entity<Domain.Entities.Venue>(new VenueConfiguration().Configure);

            modelBuilder.Entity<Domain.Entities.Match>()
                .HasOne(x => x.HomeTeam).WithMany(x => x.HomeMatches).HasForeignKey(x => x.HomeTeamId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Domain.Entities.Match>()
                .HasOne(x => x.AwayTeam).WithMany(x => x.AwayMatches).HasForeignKey(x => x.AwayTeamId).OnDelete(DeleteBehavior.Restrict);            
        }
    }
}
