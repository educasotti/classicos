﻿using Domain.Entities;
using Domain.Interfaces.Repositories;

namespace Data.Repositories
{
    public class ConfederationRepository : RepositoryBase<Confederation>, IConfederationRepository
    {
    }
}
