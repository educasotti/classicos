﻿using Data.Context;
using Domain.Interfaces;
using Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using System.Text;

namespace Data.Repositories
{
    public class RepositoryBase <TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    {
        protected string _connectionString;
        public ClassicosContext Db = new ClassicosContext(new DbContextOptionsBuilder<ClassicosContext>().UseSqlServer("Server=.\\SqlExpress;Database=ClassicosDB;User Id=sa;Password=Feb12@2019;MultipleActiveResultSets=True").Options);

        public RepositoryBase(string connectionString)
        {
            _connectionString = connectionString;
            var optionsBuilder = new DbContextOptionsBuilder<ClassicosContext>();
            optionsBuilder.UseSqlServer(connectionString);
            Db = new ClassicosContext(optionsBuilder.Options);
        }

        public RepositoryBase()
        {

        }

        public void Add(TEntity obj)
        {
            Db.Set<TEntity>().Add(obj);
            Db.SaveChanges();
        }

        public void AddMany(IEnumerable<TEntity> list)
        {
            ClassicosContext context = null;
            try
            {
                context = Db;
                context.Database.AutoTransactionsEnabled = false;

                int count = 0;
                foreach (var entityToInsert in list)
                {
                    ++count;
                    context = AddToContext(context, entityToInsert, count, list.Count(), true);
                }

                context.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (context != null)
                    context.Dispose();
            }

        }

        private ClassicosContext AddToContext(ClassicosContext context, TEntity entity, int count, int commitCount, bool recreateContext)
        {
            context.Set<TEntity>().Add(entity);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new ClassicosContext(new DbContextOptions<ClassicosContext>());
                    context.Database.AutoTransactionsEnabled = false;
                }
            }

            return context;
        }

        public TEntity GetById(int Id)
        {
            return Db.Set<TEntity>().Find(Id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Db.Set<TEntity>().ToList();
        }

        public IEnumerable<TEntity> GetMany(Expression<Func<TEntity, bool>> where)
        {
            return Db.Set<TEntity>().Where(where).ToList();
        }

        public void Update(TEntity obj)
        {
            Db.Entry(obj).State = EntityState.Modified;
            Db.SaveChanges();
        }

        public void Remove(TEntity obj)
        {
            Db.Set<TEntity>().Remove(obj);
            Db.SaveChanges();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
