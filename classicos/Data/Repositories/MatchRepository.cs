﻿using Data.Context;
using Domain.Interfaces;
using Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using System.Text;
using Domain.Entities;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class MatchRepository : RepositoryBase<Match>, IMatchRepository
    {
        public IEnumerable<Match> GetFiltered(int teamId = 0, int competitionId = 0, string status = "")
        {
            var result = Db.Matches.Include(m => m.AwayTeam).Include(m => m.Competition).Include(m => m.HomeTeam).AsQueryable();
            if (teamId > 0)
            result = result.Where(x => x.AwayTeamId == teamId || x.HomeTeamId == teamId);
            if (competitionId > 0)
                result = result.Where(x => x.CompetitionId == competitionId);
            if(!string.IsNullOrEmpty(status))
            {
                MatchStatus matchStatus;
                switch (status)
                {
                    case "finished":
                        matchStatus = MatchStatus.Finished;
                        break;
                    case "scheduled":
                        matchStatus = MatchStatus.Scheduled;
                        break;
                    default:
                        matchStatus = MatchStatus.Finished;
                        break;
                }
                result = result.Where(x => x.Status == matchStatus);
            }
            return result.Where(x => x.IsPublished);
        }
        public IEnumerable<Match> Compare(int team1, int team2)
        {
            var result = Db.Matches.Include(m => m.AwayTeam).Include(m => m.Competition).Include(m => m.HomeTeam).AsQueryable();
            if(team1 > 0)
                result = result.Where(x => x.AwayTeamId == team1 || x.HomeTeamId == team1);
            if(team2 > 0)
                result = result.Where(x => x.AwayTeamId == team2 || x.HomeTeamId == team2);
            return result.Where(x => x.IsPublished);
        }

        public Match TeamFirstMatch(int teamId)
        {
            var result = Db.Matches
                .Include(m => m.AwayTeam)
                .Include(m => m.HomeTeam)
                .AsQueryable();

            result = result.Where(x => x.AwayTeamId == teamId || x.HomeTeamId == teamId);
            if (result.Count() > 0)
                return result.Where(x => x.IsPublished).OrderBy(x => x.Date).First();
            else
                return null;
        }

        public Match TeamLastMatch(int teamId)
        {
            var result = Db.Matches
                .Include(m => m.AwayTeam)
                .Include(m => m.HomeTeam)
                .AsQueryable();

            result = result.Where(x => (x.AwayTeamId == teamId || x.HomeTeamId == teamId) && x.Status == MatchStatus.Finished);
            if (result.Count() > 0)
                return result.Where(x => x.IsPublished).OrderBy(x => x.Date).Last();
            else
                return null;
        }

        public Match TeamNextMatch(int teamId)
        {
            var result = Db.Matches
                .Include(m => m.AwayTeam)
                .Include(m => m.HomeTeam)
                .AsQueryable();

            result = result.Where(x => (x.AwayTeamId == teamId || x.HomeTeamId == teamId) && x.Status == MatchStatus.Scheduled);
            if (result.Count() > 0)
                return result.Where(x => x.IsPublished).OrderBy(x => x.Date).First();
            else
                return null;
        }

        public Match BiggestWin(int teamId)
        {
            return BiggestMatch(teamId, true);
        }

        public Match BiggestDefeat(int teamId)
        {
            return BiggestMatch(teamId, false);
        }

        public Match BiggestMatch(int teamId, bool oper)
        {
            var query = Db.Matches
                .Include(m => m.AwayTeam)
                .Include(m => m.HomeTeam)
                .AsQueryable();

            if (query.Count() == 0)
                return null;

            var result = new List<Match>();
            if(oper)
            {
                result = query
                    .Where(
                    x => (x.AwayTeamId == teamId || x.HomeTeamId == teamId)
                    && x.Status == MatchStatus.Finished
                    && (x.HomeTeamId == teamId && x.ScoreHome > x.ScoreAway || x.AwayTeamId == teamId && x.ScoreAway > x.ScoreHome)
                ).Where(x => x.IsPublished).ToList();
            }
            else
            {
                result = query.Where(
                    x => (x.AwayTeamId == teamId || x.HomeTeamId == teamId)
                    && x.Status == MatchStatus.Finished
                    && (x.HomeTeamId == teamId && x.ScoreHome < x.ScoreAway || x.AwayTeamId == teamId && x.ScoreAway < x.ScoreHome)
                ).Where(x => x.IsPublished).ToList();
            }
            if (result.Count() <= 0)
                return null;

            return result.Select(i => new {
                    match = i,
                    soma = i.ScoreHome + i.ScoreAway,
                    diff = (i.ScoreHome - i.ScoreAway >= 0 ? i.ScoreHome - i.ScoreAway : (i.ScoreHome - i.ScoreAway) * -1)
                    })
                .OrderByDescending(x => x.diff).ThenByDescending(x => x.soma)
                .ToList()
                .FirstOrDefault()
                .match;
        }

        public IEnumerable<VenueSimple> GetMatchesByLoc(string code)
        {
            return Db.Matches
                .Where(x => x.IsPublished && x.Locality.Contains(string.Format(", {0}", code)))
                .Select(s => new VenueSimple { 
                    Venue = s.Venue, 
                    Locality = s.Locality.Replace(string.Format(", {0}", code), "") 
                }).Distinct();
        }

        public IEnumerable<Match> GetHomeMatches()
        {
            var matches = Db.Matches
               .Include(m => m.AwayTeam)
               .Include(m => m.Competition)
               .Include(m => m.HomeTeam)
               .AsQueryable();

            return matches.Where(x => x.Status == MatchStatus.Scheduled)
                .OrderBy(x => x.Date)
                .Take(10);                
        }

        public IEnumerable<Match> GetMatchesByTeamByComp(int teamId, int competitionId)
        {
            return Db.Matches.Where(x => (x.AwayTeamId == teamId || x.HomeTeamId == teamId) && x.CompetitionId == competitionId);
        }

    }

    public class VenueSimple
    {
        public string Venue { get; set; }
        public string Locality { get; set; }
    }
}
