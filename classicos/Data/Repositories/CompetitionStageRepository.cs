﻿using Domain.Entities;
using Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Repositories
{
    public class CompetitionStageRepository : RepositoryBase<CompetitionStage>, ICompetitionStageRepository
    {
    }
}
