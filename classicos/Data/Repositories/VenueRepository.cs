﻿using Data.Context;
using Domain.Interfaces;
using Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using System.Text;
using Domain.Entities;
namespace Data.Repositories
{
    public class VenueRepository : RepositoryBase<Venue>, IVenueRepository
    {
        public IEnumerable<Venue> GetFiltered(int teamId = 0, int confederationId = 0, string locality = "")
        {
            var result = Db.Venues.Include(v => v.Team).Include(v => v.Team.Confederation).AsQueryable();
            if (teamId > 0)
                result = result.Where(x => x.TeamId == teamId);
            if (confederationId > 0)
                result = result.Where(x => x.Team.ConfederationId == confederationId);
            if (!string.IsNullOrEmpty(locality))
                result = result.Where(x => x.Locality == locality);
            return result;
        }

        public IEnumerable<string> GetLocalities()
        {
            return Db.Venues.Select(s => s.Locality).Distinct();
        }
    }
}
