﻿using Domain.Entities;
using Domain.Interfaces.Repositories;

namespace Data.Repositories
{
    public class CompetitionRepository : RepositoryBase<Competition>, ICompetitionRepository
    {
    }
}
