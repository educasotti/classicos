﻿using Data.Context;
using Domain.Interfaces;
using Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using System.Text;
using Domain.Entities;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class TeamRepository : RepositoryBase<Team>, ITeamRepository
    {
        public IEnumerable<Team> GetTeams(int confederationId = 0)
        {
            var result = Db.Teams.Include(m => m.Confederation).AsQueryable();
            if (confederationId > 0)
                result = result.Where(x => x.ConfederationId == confederationId);
            return result;
        }

        public Team GetTeamByCode(string code)
        {
            return Db.Teams.Where(x => x.TeamCode.ToLower() == code.ToLower()).FirstOrDefault(); 
        }
    }
}
