﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class teamestablished : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Teams_Confederations_ConfederationId",
                table: "Teams");

            migrationBuilder.AlterColumn<int>(
                name: "ConfederationId",
                table: "Teams",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EstablishedAt",
                table: "Teams",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddForeignKey(
                name: "FK_Teams_Confederations_ConfederationId",
                table: "Teams",
                column: "ConfederationId",
                principalTable: "Confederations",
                principalColumn: "ConfederationId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Teams_Confederations_ConfederationId",
                table: "Teams");

            migrationBuilder.DropColumn(
                name: "EstablishedAt",
                table: "Teams");

            migrationBuilder.AlterColumn<int>(
                name: "ConfederationId",
                table: "Teams",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Teams_Confederations_ConfederationId",
                table: "Teams",
                column: "ConfederationId",
                principalTable: "Confederations",
                principalColumn: "ConfederationId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
