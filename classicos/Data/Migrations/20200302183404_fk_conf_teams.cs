﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class fk_conf_teams : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ConfederationId",
                table: "Teams",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Teams_ConfederationId",
                table: "Teams",
                column: "ConfederationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Teams_Confederations_ConfederationId",
                table: "Teams",
                column: "ConfederationId",
                principalTable: "Confederations",
                principalColumn: "ConfederationId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Teams_Confederations_ConfederationId",
                table: "Teams");

            migrationBuilder.DropIndex(
                name: "IX_Teams_ConfederationId",
                table: "Teams");

            migrationBuilder.DropColumn(
                name: "ConfederationId",
                table: "Teams");
        }
    }
}
