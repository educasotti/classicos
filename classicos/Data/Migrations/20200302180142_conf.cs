﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class conf : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ConfederationId",
                table: "Competitions",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Confederations",
                columns: table => new
                {
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    IsPublished = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    ConfederationId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Confederations", x => x.ConfederationId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Competitions_ConfederationId",
                table: "Competitions",
                column: "ConfederationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Competitions_Confederations_ConfederationId",
                table: "Competitions",
                column: "ConfederationId",
                principalTable: "Confederations",
                principalColumn: "ConfederationId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Competitions_Confederations_ConfederationId",
                table: "Competitions");

            migrationBuilder.DropTable(
                name: "Confederations");

            migrationBuilder.DropIndex(
                name: "IX_Competitions_ConfederationId",
                table: "Competitions");

            migrationBuilder.DropColumn(
                name: "ConfederationId",
                table: "Competitions");
        }
    }
}
