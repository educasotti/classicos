﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class AETPK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "AET",
                table: "Matches",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "PK",
                table: "Matches",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "PKScore",
                table: "Matches",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AET",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "PK",
                table: "Matches");

            migrationBuilder.DropColumn(
                name: "PKScore",
                table: "Matches");
        }
    }
}
