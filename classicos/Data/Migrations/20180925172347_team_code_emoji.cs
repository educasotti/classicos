﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class team_code_emoji : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "EmojiFlag",
                table: "Teams",
                maxLength: 10,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TeamCode",
                table: "Teams",
                maxLength: 3,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EmojiFlag",
                table: "Teams");

            migrationBuilder.DropColumn(
                name: "TeamCode",
                table: "Teams");
        }
    }
}
