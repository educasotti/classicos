﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interfaces.Repositories
{
    public interface IMatchRepository : IRepositoryBase<Match>
    {
        IEnumerable<Match> GetFiltered(int teamId, int competitionId, string status);
        IEnumerable<Match> Compare(int team1, int team2);
        Match TeamFirstMatch(int teamId);
        Match TeamLastMatch(int teamId);
        Match TeamNextMatch(int teamId);
        Match BiggestWin(int teamId);
        Match BiggestDefeat(int teamId);
        Match BiggestMatch(int teamId, bool oper);
    }
}
