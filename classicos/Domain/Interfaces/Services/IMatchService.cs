﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;

namespace Domain.Interfaces.Services
{
    public interface IMatchService : IServiceBase<Match>
    {
    }
}
