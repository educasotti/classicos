﻿using Domain.Entities;

namespace Domain.Interfaces.Services
{
    public interface ITeamService : IServiceBase<Team>
    {
    }
}
