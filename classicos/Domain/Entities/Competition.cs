﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Competition : CommonProps
    {
        public int CompetitionId { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
        public string HomeBanner { get; set; }
        public string MatchBanner { get; set; }

        public IEnumerable<Match> Matches { get; set; }
    }
}
