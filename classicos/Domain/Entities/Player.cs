﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Player : CommonProps
    {
        public int PlayerId { get; set; }
        public string Name { get; set; }
        public int ConfederationId { get; set; }
        public virtual Confederation Confederation { get; set; }
    }
}
