﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Manager : CommonProps
    {
        public int ManagerId { get; set; }
        public string Name { get; set; }
    }
}
