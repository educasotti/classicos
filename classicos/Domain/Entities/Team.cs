﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class Team: CommonProps
    {
        public int TeamId { get; set; }
        public string Name { get; set; }
        public string Crest { get; set; }
        public string EmojiFlag { get; set; }
        [MaxLength(3,ErrorMessage ="Max 3")]
        public string TeamCode { get; set; }
        public string FullName { get; set; }
        public string NickName { get; set; }
        public string HomeAt { get; set; }
        public DateTime EstablishedAt { get; set; }
        public int ConfederationId { get; set; }
        public virtual Confederation Confederation { get; set; }

        public IEnumerable<Match> HomeMatches { get; set; }
        public IEnumerable<Match> AwayMatches { get; set; }
        public IEnumerable<Venue> Venues { get; set; }

    }
}
