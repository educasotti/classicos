﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class CommonProps
    {
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public bool IsPublished { get; set; }
        public bool IsDeleted { get; set; }
    }
}
