﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
    public class Match : CommonProps
    {
        public int MatchId { get; set; }        
        public int HomeTeamId { get; set; }        
        public int AwayTeamId { get; set; }
        public virtual Team HomeTeam { get; set; }
        public virtual Team AwayTeam { get; set; }

        public int ScoreHome { get; set; }
        public int ScoreAway { get; set; } 
        public DateTime Date { get; set; }
        public int CompetitionId { get; set; }
        public string Stage { get; set; }
        public string Venue { get; set; }
        public string Locality { get; set; }
        public bool AET { get; set; }
        public bool PK { get; set; }
        public string PKScore { get; set; }

        public MatchStatus Status { get; set; }

        public virtual Competition Competition { get; set; }

        public Match()
        {
            if (Status == MatchStatus.Scheduled)            
                ScoreAway = ScoreHome = 0;                
        }


    }

    public enum MatchStatus
    {
        Scheduled, Finished
    }
}
