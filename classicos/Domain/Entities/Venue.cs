﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Venue : CommonProps
    {
        public int VenueId { get; set; }
        public string Name { get; set; }
        public string Locality { get; set; }
        public int Capacity { get; set; }
        public int TeamId { get; set; }
        public virtual Team Team { get; set; }
    }
}
