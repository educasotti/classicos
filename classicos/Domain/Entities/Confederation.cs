﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class Confederation : CommonProps
    {
        public int ConfederationId { get; set; }
        public string Name { get; set; }
        public IEnumerable<Competition> Competitions { get; set; }
        public IEnumerable<Team> Teams { get; set; }
    }
}
