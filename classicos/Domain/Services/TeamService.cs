﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;
using Domain.Interfaces;
using Domain.Interfaces.Repositories;

namespace Domain.Services
{
    public class TeamService : ServiceBase<Team>, Interfaces.Services.ITeamService
    {
        private readonly ITeamRepository _teamRepository;

        public TeamService(ITeamRepository teamRepository)
            : base(teamRepository)
        {
            _teamRepository = teamRepository;
        }
    }
}
