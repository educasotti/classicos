﻿using System;
using System.Collections.Generic;
using System.Text;
using Domain.Entities;
using Domain.Interfaces;
using Domain.Interfaces.Repositories;

namespace Domain.Services
{
    public class MatchService : ServiceBase<Match>, Interfaces.Services.IMatchService
    {
        private readonly IMatchRepository _matchRepository;
        public MatchService(IMatchRepository matchRepository)
            :base(matchRepository)
        {
            _matchRepository = matchRepository;
        }
        

    }
}
